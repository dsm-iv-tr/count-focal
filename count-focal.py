#!/usr/bin/python

# parse a directory of JPEG files, look at EXIF data, and spit out the focal lengths used,
# plus how many photos were taken at that focal length.
# Troy Rennie
# dsm.iv.tr@gmail.com

# public domain code, do what you want

import os
import argparse
import sys

from PIL import Image
from PIL.ExifTags import TAGS

def get_exif_field(exifdata, fieldname):
	for (key, val) in exifdata.iteritems():
		if TAGS.get(key) == fieldname:
			return val

def sort_fls(fls):
	# since we use .items() in our sorted() call, this will sort on values (default ascending sort)
	return fls[1]

focal_length = {}
manufacturer = {}
fcount = 0

parseobj = argparse.ArgumentParser()
parseobj.add_argument('-i', '--input', help='input files or directory', default='.')
parseobj.add_argument('-o', '--output', help='output file, default is STDOUT')

arguments = parseobj.parse_args()

if arguments.input == None:
	parseobj.print_help()
	sys.exit(1)

if arguments.output != None:
	print "NOTICE: Sending output to", arguments.output + "."

	if os.path.isfile(arguments.output) != False:
		sys.stdout = open(arguments.output, 'w')
	else:
		print "NOTICE: File exists, not allowing you to potentially overwrite data. Continuing on STDOUT!\n"

for infile in os.listdir(arguments.input):
	fullpath = os.path.join(arguments.input, infile)

	if "jpg" in str(fullpath.lower()):
		img = Image.open(fullpath)
		
		if hasattr(img, '_getexif'):
			imgexif = img._getexif()
			fcount += 1
		
			if imgexif != None:
				fl = get_exif_field(imgexif, 'FocalLength')

				if fl != None:
					if fl[0] in focal_length:
						focal_length[fl[0]] += 1
					else:
						focal_length[fl[0]] = 1
				
sortfls = sorted(focal_length.items(), key=sort_fls, reverse=True)

if fcount >= 1:
	print "Found EXIF within", fcount, "JPEG files in", arguments.input + ".\n"

	for flength in sortfls:
		print "==========================\n"
		print "FocalLength =", flength[0]
		print "TOTAL =", flength[1]
		print "==========================\n"
		
	sys.exit(0)

else:
	print "No files with EXIF data for FocalLength found in", arguments.input + "."
	sys.exit(1)
